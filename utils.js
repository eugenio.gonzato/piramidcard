function generateDeck(semi) {
  // funzione che genera il mazzo di carte
  // usando l'array 'semi' per i semi
  // e valori che vannno da 1 a 13
  for (let v = 1; v <= 13; v++) {
    for (let s = 0; s < 4; s++) {
      let imgcart = document.getElementById(
        `${v}${semi[s]}`
      );
      let cart = new Carta(semi[s], v, imgcart);
      deck.push(cart);
    }
  }
}

function calcCardPos (gamedata) {
  // funzione che calcola la posizione delle carte nella piramide
  // e assegna le coordinate x ed y
  let offsetX = Math.floor((gamedata.tableW - (gamedata.cardW * gamedata.lineOneCard)) / (gamedata.lineOneCard + 1));
  let offsetY = 5;
  let piramidN = 3;
  let positions = [[], [], [], []];
  let x0 = 0;
  let gep = offsetX + gamedata.cardW;
  
  // fourth line
  x0 = gamedata.cardW * 2 + offsetX * 2.5;
  for (let i = 0; i < piramidN; i++) {
    positions[3].push(
      {
        x: x0 + gep * 3 * i,
        y: gamedata.cardH / 2
      }
    );
  }

  // third line
  x0 = gamedata.cardW * 1.5 + offsetX *2;
  for (let i = 0; i < piramidN; i++) {
    for (let j = 0; j < 2; j++) {
      positions[2].push(
        {
          x: x0 + gep * j,
          y: gamedata.cardH + offsetY
        }
      );
    }
    //--
    x0 += gep * 3;
  }

  // second line
  x0 = gamedata.cardW + offsetX + offsetX / 2;
  for (let i = 0; i < piramidN; i++) {
    for (let j = 0; j < 3; j++) {
      positions[1].push(
        {
          x: x0 + gep * j,
          y: 1.5 * gamedata.cardH + 2 * offsetY
        }
      );
    }
    //--
    x0 += gep * 3;
  }

  // first line
  x0 = offsetX + gamedata.cardW / 2;
  for (let i = 0; i < gamedata.lineOneCard; i++) {
    positions[0].push(
      {
        x: x0 + gep * i,
        y: 2 * gamedata.cardH + 3 * offsetY
      }
    );
  }

  return positions;
}

// funzione per mescolare le carte
function mescolaCarte (array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

function givePosToCard (positions, deck) {
  // funzione che associa alle carte 'deck' le coordinate 'position' calcolare
  // con la funzione calcCardPos
  let count = 0;
  for (let i = positions.length -1; i >= 0; i--) {
    for (let j = 0; j < positions[i].length; j++) {
      deck[count].x = positions[i][j].x;
      deck[count].y = positions[i][j].y;
      count += 1;
    }
  }
}

function drawAll (deck, ctx) {
  // funzione che disegna tutte le carte del mazzo 'deck' che sono quelle che
  // costituiscono la piramide
  for (let i = 0; i < deck.length; i++) {
    deck[i].drawCart(ctx);
  }
}

function createInGameDeck(deck) {
  // funzione che separa il mazzo di carte originale 'deck' che rimangono le carte
  // della piramide e restituisce le carte che andranno a formare le carte in gioco.
  // adesso che la guardo senza commenti non si capisce che cazzo serve
  return deck.splice(28, 24);
}

function givePositionToDeckInGame(deckInGame, gamedata, deck) {
  // funzione che assegna le coordinate alle carte del mazzetto in gioco
  // quelle coperte che devono essere girate per farlo va a cercare la carta
  // piu bassa della piramide e aggiunge una quantità: quella sarà la y
  // Per la x legge i gamedata e rispetto alla larghezza del canvas le mette
  // in centro un po' a sinistra. anche questa è una funzione che senza commento
  // non si capisce un cazzo
  let posy = 0;
  let posx = 0;
  for (let i = 0; i < deck.length; i++) {
    // cerca la carta più bassa con un y maggiore.
    if (deck[i].y > posy) {
      posy = deck[i].y;
    }
  }
  // aggiunge alla carta piu bassa una quantità per definire la y del mazzetto
  // e la x la calcola in base alla larghezza del cavas 'tableW'
  posy += gamedata.cardH + 30;
  posx = gamedata.tableW / 2 - gamedata.cardW;

  for (let i = 0; i < deckInGame.length; i++) {
    // associa a tutte le carte del mazzetto la stessa coordinata perchè sono
    // una sopra l'altra e si assicura che le carte siano coperto impostando 
    // la proprietà 'isOpen' to false.
    deckInGame[i].y = posy;
    deckInGame[i].x = posx;
    deckInGame[i].isOpen = false;
  }
}

function drawCardInGame (card, ctx) {
  // disegna tutte le carte del mazzo 'card' sul contest del canvas 'ctx'
  for (let i = 0; i < card.length; i++) {
    card[i].drawCart(ctx);
  }
}

function drawCardPlayed (card, ctx) {
  // disegna tutte le carte già girate , appunto played, questa funzione
  // verifica prima che il mazzo non sia vuoto, si potrebbe fare con la
  // funzione sopra ma anche questa funzione fa schifo
  if (card.length > 0) {
    for (let i = 0; i < card.length; i++) {
      card[i].drawCart(ctx);
    }
  }
}

function setIddenCard(deck) {
  // questa funzione imposta su falso la proprietà isOpen a tutte
  // le carte della piramide che non appartendono alla unltima fila
  // serve all'inizio del gioco quando vìene creata la piramide inizialmnete
  // tutte le carte della piramide sono coperte ma poi serve scoprire quelle
  // della prima fila per iniziare il gioco.
  let lastRowY = 0;
  for (let i = 0; i < deck.length; i++) {
    if (deck[i].y > lastRowY) {
      lastRowY = deck[i].y
    }
  }
  for (let i = 0; i < deck.length; i++) {
    if (deck[i].y < lastRowY) {
      deck[i].isOpen = false;
    }
  }
}

function cardFlip (deckInGame, deckPlayed) {
  // 'deckInGame' carte in attesa coperte
  // 'deckPlayed' carte in attesa scoperte
  // funzione usata quanto si clicca sul mazzo delle carte in attesa coperte.
  // se ce ne sono ancora gira la carta e la mette sopra il mazzetto delle carte
  // in attesa scoperte, questa sarà la prossima carta in attesa.
  if (deckInGame.length > 0) {
    let tcard = deckInGame.pop(); // toglie una carta da una parte
    // TODO animazione della catre
    let startXPosition = tcard.x;
    let endXPosition = GAMEDATA.tableW / 2 + GAMEDATA.cardW;
    let middleXPosition = (startXPosition + endXPosition) / 2;
    console.log('start x = ' + startXPosition + ' end x = ' + endXPosition);
    // funzione per animare il movimento della carte
    function xApdate(){
      setTimeout(()=>{
        startXPosition += 20;
        if (startXPosition < middleXPosition && tcard.w > 1) {
          tcard.w -= 10;
        }
        tcard.x = startXPosition;
        if (startXPosition > middleXPosition) {
          tcard.isOpen = true;          // la scopre
          tcard.w = 50;
        }
        if (startXPosition < endXPosition) {
          // la funzione viene richiamata fino a quando non viene raggiunta la quota
          // finale della x 'endXPosition'
          xApdate();
        }
      }, 30);
    }
    xApdate();

    tcard.x = GAMEDATA.tableW / 2 + GAMEDATA.cardW; // gli assegna le coordinate dell'altro mazzetto
    deckPlayed.push(tcard); // aggiunge la carta all mazzetto delle carte scoperte.
  } else {
    console.log("Carte finite!!");
  }
}

function isInFlip (point, deckInGame) {
  // Funzione che verifica se il punto dato 'point' è
  // sopra una carta girata della piramide 'deckInGame'
  // restituisce 'true' o 'false'
  if (deckInGame.length > 0) {
    let x0 = deckInGame[0].x - deckInGame[0].w /2;
    let x1 = deckInGame[0].x + deckInGame[0].w /2;
    let y0 = deckInGame[0].y - deckInGame[0].h /2;
    let y1 = deckInGame[0].y + deckInGame[0].h /2;
    if (point.x > x0 && point.x < x1 && point.y > y0 && point.y < y1) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function isInOpenCard (point, deck) {
  // Funzione che verifica se il punto dato 'point'
  // si trova sul su una carta scoperta del mazzo 'deck' che rappresenta il 
  // gruppo delle carte nella piramide.
  if (deck.length > 0) {
    for (let i = 0; i < deck.length; i++) {
      let c = deck[i];
      if (c.isOpen === true) {
        let x0 = c.x - c.w / 2;
        let x1 = c.x + c.w / 2;
        let y0 = c.y - c.h / 2;
        let y1 = c.y + c.h / 2;
        if (point.x > x0 && point.x < x1 && point.y > y0 && point.y < y1) {
          if (i === 0) {
            return 'last';
          } else {
            return i;
          }
        }
      }
    }
  }
  return false;
}

function playCard (point, deck, deckPlayed) {
  // Funzione principale che fa prendere le carte dalla piramide
  // 'point' è il punto cliccato, deck sono le carte nella piramide e
  // 'deckPlayed' è il mazzo dele carte prese.
  if (deckPlayed.length < 1) {
    // Verifica che ci sia almeno una carta girata altrimenti esce dalla funzione
    return
  }

  let n = isInOpenCard(point, deck);
  if (n === 'last') {
    n = 0;
  } else {
    n = n;
  }
  console.log(n);
  // assegna ad 'n' la carta cliccata e stampa sulla console
  console.log(`Hai clieccato la carta con seme: ${deck[n].seme} e valore ${deck[n].valore}`)
  
  // controllo se è una carta valida che può essere presa dalla piramide
  // confrontando il suo valore con il valore della carta mostrata sul 
  // mazzo delle carte prese. 
  let valCPlay = deck[n].valore; // valore della carta cliccata
  let valCAtt = deckPlayed[deckPlayed.length - 1].valore; // valore della carta in attesa

  console.log(`-${valCPlay}--${valCAtt}`); 

  if (valCAtt == (valCPlay + 1) || valCAtt == (valCPlay - 1)) {
    // se il valore della carta cliccata è maggiore o minore di uno rispetto alla carta in attesa
    // la carta cliccata viene tolta dalla piramide e messa sul mazzo delle carte in giocate
    // costituendo la prossima carta in attesa.
    let c = deck.splice(n, 1)[0];
    c.x = deckPlayed[0].x;
    c.y = deckPlayed[0].y;
    deckPlayed.push(c);
    let pp = parseInt(document.getElementById("punteggio").innerHTML)
    document.getElementById("punteggio").innerHTML = (pp+1);
    document.getElementById("messaggio").innerHTML = "Bravo";
    checkOpenCard(deck);
  } else {
    document.getElementById("messaggio").innerHTML = "Hai scelto una carta non valida";
  }
}

function checkOpenCard(deck) {
  // Questa funzione passa tutte le carte nella piramide e verifiche quali sono scoperte
  // Se ne trova di scoperte modifica la proprietà 'isOpen' in true della carta trovata scoperta 
  // Questa funzione viene eseguita ogni volta che una carta della piramide viene giocata in modo
  // da verificare se giocando quella carta si sono scoperte altre carte e quindi mettendo la proprietà
  // isOpen su true delle carte diventate scoperte quando la piramide viene ridisegnata le carte scoperte
  // vengono mostrate mentre prima erano nascoste.
  for (let i = 0; i < deck.length; i++) {
    let list = 0
    // Itera in tutte le carte della piramide e per ogni iterazione crea una lista che contiene il numero delle
    // carte che coprono la carta in esame. Questo viene fatto controllando per ogni carte tutte le altre
    // carte della piramide.
    for (let j = 0; j < deck.length; j++) {
      if ( deck[j].y > deck[i].y ) {
        // per accorciare la ricerca prende in considerazione solo le carte sotto, con una y maggiore
        // perchè solo quelle carte possono coprire la carte in esame.
        if ((Math.abs(deck[j].x - deck[i].x) < deck[i].w) &&
            (Math.abs(deck[j].y - deck[i].y) < deck[i].h) &&
            (Math.abs(deck[j].y - deck[i].y) != 0))
        {
          // se viene trovata una carta che copre la carta in esame la quantità delle carte
          // '' coprennti '' viene aumentata di uno.
          list += 1;
        }
      }
    }
    // se la quantità delle carte '' coprenti'' è zero significa che la carta in esame è diventata
    // libera e viene scoperta impostando la sua proprietà 'isOpen' su true.
    if (list === 0) { deck[i].isOpen = true; }
  }
}
